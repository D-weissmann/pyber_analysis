## Pyber Analysis

# Overview 
This analysis is to understand how riders in different types of cities use ride sharing. 

# Analysis
As you can see in the below table because of the number of rides and riders is so much higher in Urban areas vs rides in more Rural areas you can see that Urban drivers make less money per ride and that the inverse is also true.
![Summary Table](https://gitlab.com/D-weissmann/pyber_analysis/-/blob/main/Summary_Table.png)
From this graph you can see that the amount of fares remains relatively consistent over the course of q1 depending on the type of city with fewer fares being gathered as the city gets more Rural
![Fares per City Type](https://gitlab.com/D-weissmann/pyber_analysis/-/blob/main/Fares_per_City.png)

#Summary
I suggest doing 3 things
    1. Because the average ride is more expensive the more rural you go this might be turning off customers, perhaps lowering the price of rides in rural areas will give them more reason to ride.
    2. Maybe the opposite is true, increasing the fares in Rural areas might incentivize more drivers to drive in Urban areas thus improving the service and making more riders use it. 
    3. Perhaps putting more of your eggs in the fruitful basket of the Urban areas is more important focus on the urban areas to improve your bottom dollar. 
